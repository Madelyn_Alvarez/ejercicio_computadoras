package computadora;

public class Computadora {

    public static void main(String[] args) {
        
        
        Fabrica F = new Fabrica();
        System.out.println(F.getDetalles());
        
        
        Tamaño T = new Tamaño();
        System.out.println(T.getDetalles());
        
        
        Color C = new Color();
        System.out.println(C.getDetalles());
        
        
        SistemaOperativo SO = new SistemaOperativo();
        System.out.println(SO.getDetalles());
        
        
        DiscoDuro DD = new DiscoDuro();
        System.out.println(DD.getDetalles());
        
        
        MemoriaRam MR = new MemoriaRam();
        System.out.println(MR.getDetalles());
        
        Espacio ES = new Espacio();
        System.out.println(ES.getDetalles());
        
   //FRABRICA 2
   
        Fabrica2 F2 = new Fabrica2();
        System.out.println(F2.getDetalles());
        
        Tamaño2 T2 = new Tamaño2();
        System.out.println(T2.getDetalles());
        
        
        Color2 C2 = new Color2();
        System.out.println(C2.getDetalles());
        
        
        SistemaOperativo2 SO2 = new SistemaOperativo2();
        System.out.println(SO2.getDetalles());
        
        
        DiscoDuro2 DD2 = new DiscoDuro2();
        System.out.println(DD2.getDetalles());
        
        
        MemoriaRam2 MR2 = new MemoriaRam2();
        System.out.println(MR2.getDetalles());
        
        Espacio2 ES2 = new Espacio2();
        System.out.println(ES2.getDetalles());
        
     // FABRICA 3
   
        Fabrica3 F3 = new Fabrica3();
        System.out.println(F3.getDetalles());
        
        Tamaño3 T3 = new Tamaño3();
        System.out.println(T3.getDetalles());
        
        
        Color3 C3 = new Color3();
        System.out.println(C3.getDetalles());
        
        
        SistemaOperativo3 SO3 = new SistemaOperativo3();
        System.out.println(SO3.getDetalles());
        
        
        DiscoDuro3 DD3 = new DiscoDuro3();
        System.out.println(DD3.getDetalles());
        
        
        MemoriaRam3 MR3 = new MemoriaRam3();
        System.out.println(MR3.getDetalles());
        
        Espacio3 ES3 = new Espacio3();
        System.out.println(ES3.getDetalles());
    
           
    }
    
    
     public static abstract class Detalles {


        abstract String getDetalles();

    }
   
     public static class Fabrica extends Detalles {


        public String getDetalles() {
            return "Fabricante: DELL";
        }

    }
     
    
    public static class Tamaño extends Detalles {


        public String getDetalles() {
            return "Tamaño: Microcomputadora";
        }

    }
    
    public static class Color extends Detalles {


        public String getDetalles() {
            return "Color: Black and White";
        }

    }
    
    public static class SistemaOperativo extends Detalles {


        public String getDetalles() {
            return "S.O: Windows 10";
        }

    }
    
    public static class DiscoDuro extends Detalles {


        public String getDetalles() {
            return "Tipo de HDD: Disco duro SDD";
        }

    }
    
    public static class MemoriaRam extends Detalles {


        public String getDetalles() {
            return "Tipo Memoria Ram: DRAM (Dynamic RAM)";
        }

    }
    
   public static class Espacio extends Detalles {


        public String getDetalles() {
            return "____________________________________";
        }

    }
   
   //FABRICA 2
    
   public static class Fabrica2 extends Detalles {


        public String getDetalles() {
            return "Fabricante: Apple";
        }

    }
   
   public static class Tamaño2 extends Detalles {


        public String getDetalles() {
            return "Tamaño: Microcomputadora";
        }

    }
    
    public static class Color2 extends Detalles {


        public String getDetalles() {
            return "Color: Silver";
        }

    }
    
    public static class SistemaOperativo2 extends Detalles {


        public String getDetalles() {
            return "S.O: Mac OS X";
        }

    }
    
    public static class DiscoDuro2 extends Detalles {


        public String getDetalles() {
            return "Tipo de HDD: Disco duro SCSI";
        }

    }
    
    public static class MemoriaRam2 extends Detalles {


        public String getDetalles() {
            return "Tipo Memoria Ram: DRAM (Dynamic RAM)";
        }

    }
    
   public static class Espacio2 extends Detalles {


        public String getDetalles() {
            return "____________________________________";
        }

    }

 //FABRICA 3
    
   public static class Fabrica3 extends Detalles {


        public String getDetalles() {
            return "Fabricante: ACER";
        }

    }
   
   public static class Tamaño3 extends Detalles {


        public String getDetalles() {
            return "Tamaño: Microcomputadora";
        }

    }
    
    public static class Color3 extends Detalles {


        public String getDetalles() {
            return "Color: Orchid Gray";
        }

    }
    
    public static class SistemaOperativo3 extends Detalles {


        public String getDetalles() {
            return "S.O: Windows 7";
        }

    }
    
    public static class DiscoDuro3 extends Detalles {


        public String getDetalles() {
            return "Tipo de HDD: Disco duro SDD";
        }

    }
    
    public static class MemoriaRam3 extends Detalles {


        public String getDetalles() {
            return "Tipo Memoria Ram: SRAM (Static RAM)";
        }

    }
    
   public static class Espacio3 extends Detalles {


        public String getDetalles() {
            return "____________________________________";
        }

    }
   
}
